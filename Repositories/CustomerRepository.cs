﻿using Chinook.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Repositories
{
    class CustomerRepository : ICustomerRepository
    {
        // 1
        /*
         Gets a list of all customers and returns the list
        Return sql exception
         * 
         */
        public List<Customer> getAllCustomer()
        {

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            List<Customer> customersList = new List<Customer>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.Id = reader.GetInt32(0);
                                temp.firstname = reader.GetString(1);
                                temp.lastname = reader.GetString(2);
                                temp.country = reader.GetString(3);

                                // Checks if field is set to null
                                if (!reader.IsDBNull(4))
                                {
                                    temp.postalCode = reader.GetString(4);
                                } else
                                {
                                    temp.postalCode = "";
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    temp.phoneNumber = reader.GetString(5);
                                } else
                                {
                                    temp.phoneNumber = "";
                                }
                               
                                
                                temp.email = reader.GetString(6);
                                customersList.Add(temp);

                            }
                        }
                    }
                }
            } 

            catch(SqlException e)
            {
                Console.WriteLine(e);
            }

            return customersList;
        }


        // 2
        /*
         * Gets a specific customer based on id input
         * Returns a customer
         * Return sql exception
         * 
         */
        public Customer getCustomer(string id)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer"+
                " WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using(SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);

                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                customer.Id = reader.GetInt32(0);
                                customer.firstname = reader.GetString(1);
                                customer.lastname = reader.GetString(2);
                                customer.country = reader.GetString(3);

                                if (!reader.IsDBNull(4))
                                {
                                    customer.postalCode = reader.GetString(4);
                                }
                                else
                                {
                                    customer.postalCode = "";
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    customer.phoneNumber = reader.GetString(5);
                                }
                                else
                                {
                                    customer.phoneNumber = "";
                                }

                                customer.email = reader.GetString(6);
                            }
                        }

                        
                    }
                }
            } 
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return customer;
        }

        // 3
        /*
         * Gets a customer by name, Returns customer object
         * Return sql exception
         * 
         */
        public Customer getCustomerByName(string name)
        {
            Customer customer = new Customer();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE FirstName LIKE @CustomerName";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerName", name);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                customer.Id = reader.GetInt32(0);
                                customer.firstname = reader.GetString(1);
                                customer.lastname = reader.GetString(2);
                                customer.country = reader.GetString(3);

                                // Checks if field is set to null
                                if (!reader.IsDBNull(4))
                                {
                                    customer.postalCode = reader.GetString(4);
                                }
                                else
                                {
                                    customer.postalCode = "";
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    customer.phoneNumber = reader.GetString(5);
                                }
                                else
                                {
                                    customer.phoneNumber = "";
                                }

                                customer.email = reader.GetString(6);
                            }
                        }


                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customer;
        }
        // 4
        /*
         * Gets 10 customers ordered by firstname, but skips the first 2 in database. Returns customer list. LIMIT did not work, used fetch
         * Return sql exception
         * 
         */
        public List<Customer> getLimit()
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY FirstName OFFSET 2 ROWS FETCH NEXT 10 ROWS ONLY";
            List<Customer> customersList = new List<Customer>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.Id = reader.GetInt32(0);
                                temp.firstname = reader.GetString(1);
                                temp.lastname = reader.GetString(2);
                                temp.country = reader.GetString(3);

                                // Checks if field is set to null
                                if (!reader.IsDBNull(4))
                                {
                                    temp.postalCode = reader.GetString(4);
                                }
                                else
                                {
                                    temp.postalCode = "";
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    temp.phoneNumber = reader.GetString(5);
                                }
                                else
                                {
                                    temp.phoneNumber = "";
                                }


                                temp.email = reader.GetString(6);
                                customersList.Add(temp);

                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customersList;
        }

        // 5
        /*
         * Adds new customer, returns bool
         * Return sql exception
         */
        public bool addNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                " VALUES(@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using(SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.firstname);
                        cmd.Parameters.AddWithValue("@LastName", customer.lastname);
                        cmd.Parameters.AddWithValue("@Country", customer.country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.postalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.phoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            } catch(SqlException e)
            {
                Console.WriteLine(e);
            }
            return success;
        }

        // 6
        /* updates existing customers name based on their id
         * returns bool
         * Return sql exception
           We assumed that showing that we can update only one field is adequate
           */

        public bool updateCustomer(string name, string id )
        {
            bool success = false;
            string sql = "UPDATE Customer " + "SET FirstName = @FirstName" + " WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        cmd.Parameters.AddWithValue("@CustomerId", id);


                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return success;
        }

        // 7
        /*
         * Gets list of countries based on how many customers from country
         * Returns customercountry list
         * Return sql exception
         */

        public List<CustomerCountry> getCountryCount()
        {
            string sql = "SELECT Country, COUNT(*) AS Count FROM Customer GROUP BY Country ORDER BY COUNT(*) DESC;";
            List<CustomerCountry> customerCountries = new List<CustomerCountry>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                temp.countryName = reader.GetString(0);
                                temp.countryCount = reader.GetInt32(1);


                                customerCountries.Add(temp);

                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customerCountries;

        }

        // 8
        /*
         * Gets customers who has spent the highest ammount of money. Returns list of customerspender
         * Return sql exception
         */
        public List<CustomerSpender> getBigSpender()
        {
            string sql = "SELECT FirstName, SUM(Total) AS Total FROM Customer, Invoice WHERE Customer.CustomerId = Invoice.CustomerId GROUP BY FirstName ORDER BY SUM(Total) DESC;";
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.spenderName = reader.GetString(0);
                                temp.spenderTotal = Convert.ToDouble(reader.GetDecimal(1));


                                customerSpenders.Add(temp);

                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customerSpenders;

        }

        // 9
        /*
         * Gets a customers most popular genre, in case of a tie both are displayed. Return list of CustomerGenre. Use nr 12 to test a tie
         * Return sql exception
         */
        public List<CustomerGenre> getPopularGenre(int id)
        {
            string sql = "SELECT TOP (1) WITH TIES Genre.Name, COUNT(Genre.Name) AS Antall  FROM Invoice, InvoiceLine, Track, Genre" +
                " WHERE @customerId = Invoice.CustomerId AND Invoice.InvoiceId = InvoiceLine.InvoiceId AND InvoiceLine.TrackId = Track.TrackId AND Track.GenreId = Genre.GenreId GROUP BY Genre.Name" +
                " ORDER BY COUNT(Genre.Name) DESC;";
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnection()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@customerId", id);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.genreName = reader.GetString(0);
                                temp.genreCount = reader.GetInt32(1);


                                customerGenres.Add(temp);

                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return customerGenres;

        }

    }


}


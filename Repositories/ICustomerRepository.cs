﻿using Chinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Repositories
{
    interface ICustomerRepository
    {
        public List<Customer> getAllCustomer();
        public Customer getCustomer(string id);
        public Customer getCustomerByName(string name);
        public List<Customer> getLimit();
        public bool addNewCustomer(Customer customer);
        public bool updateCustomer(string name, string id);
        public List<CustomerCountry> getCountryCount();
        public List<CustomerSpender> getBigSpender();
        public List<CustomerGenre> getPopularGenre(int id);
    }
}

﻿using Microsoft.Data.SqlClient;
using System;
using Chinook.Repositories;
using System.Collections.Generic;
using Chinook.Models;

namespace Chinook
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository ny = new CustomerRepository();

             void getallCustomers()
            {
                List<Customer> listen = ny.getAllCustomer();

                listen.ForEach(cust =>
                {
                    Console.WriteLine("Id: " + cust.Id + " Firstname: " + cust.firstname + " Lastname: " + cust.lastname + " Country: " + cust.country + " Postalcode: " + cust.postalCode + " phonenumber: " + cust.phoneNumber + " Email: " + cust.email + "\n");
                });
            }


            
            void getCustomer()
            {
                Customer cust = ny.getCustomer("1");

                Console.WriteLine("Id: " + cust.Id + " Firstname: " + cust.firstname + " Lastname: " + cust.lastname + " Country: " + cust.country + " Postalcode: " + cust.postalCode + " phonenumber: " + cust.phoneNumber + " Email: " + cust.email + "\n");

            }

           

            void getNameCustomer()
            {
                Customer cust = ny.getCustomerByName("Bjørn");

                Console.WriteLine("Id: " + cust.Id + " Firstname: " + cust.firstname + " Lastname: " + cust.lastname + " Country: " + cust.country + " Postalcode: " + cust.postalCode + " phonenumber: " + cust.phoneNumber + " Email: " + cust.email + "\n");

            }


            void getLimitOffset()
            {
                List<Customer> listen = ny.getLimit();

                listen.ForEach(cust =>
                {
                    Console.WriteLine("Id: " + cust.Id + " Firstname: " + cust.firstname + " Lastname: " + cust.lastname + " Country: " + cust.country + " Postalcode: " + cust.postalCode + " phonenumber: " + cust.phoneNumber + " Email: " + cust.email + "\n");
                });

            }

           


            void createNew()
            {
                Customer customer = new Customer();

                customer.firstname = "Navn";
                customer.lastname = "etternavn";
                customer.country = "USA";
                customer.postalCode = "785hf1234";
                customer.phoneNumber = "447597746";
                customer.email = "Email";

                bool created = ny.addNewCustomer(customer);
                Console.WriteLine(created);

            }

            
            void upDateCustomer()
            {
                bool updated = ny.updateCustomer("Hans", "60");

                Console.WriteLine(updated);
            }


            void getCountCountries()
            {
                List<CustomerCountry> countries = ny.getCountryCount();

                countries.ForEach(count =>
                {
                    Console.WriteLine("Country: " + count.countryName + " Customers: " + count.countryCount);
                });
            }

            void getSpenders()
            {
                List<CustomerSpender> spender = ny.getBigSpender();

                spender.ForEach(count =>
                {
                    Console.WriteLine("Name: " + count.spenderName + " Total: " + count.spenderTotal);
                });
            }

            void getGenres()
            {
                List<CustomerGenre> genre = ny.getPopularGenre(12);

                genre.ForEach(gen =>
                {
                    Console.WriteLine("Genre: " + gen.genreCount + " Total: " + gen.genreCount);
                });
            }

            /*
            Commented out all functions

            getallCustomers();
            getCustomer();
            getNameCustomer();
            getLimitOffset();
            createNew();
            upDateCustomer();
            getCountCountries();
            getSpenders();
            getGenres();

            */
        }





    }
}

USE SuperheroesDb;

INSERT INTO Superhero (Name, Alias, Origin) VALUES('Tony Stark', 'Iron Man', 'New York');
INSERT INTO Superhero (Name, Alias, Origin) VALUES('Bruce Wayne', 'Batman', 'Gotham');
INSERT INTO Superhero (Name, Alias, Origin) VALUES('Tchalla', 'Black Panther', 'Wakanda');
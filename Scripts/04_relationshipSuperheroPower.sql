USE SuperheroesDb;

CREATE TABLE HeroPower(
	heroId INT NOT NULL,
	powerId INT NOT NULL,

	CONSTRAINT fk_hero FOREIGN KEY (heroId)
	REFERENCES Superhero(Id),
	CONSTRAINT fk_power FOREIGN KEY (powerId)
	REFERENCES Power(Id),
	PRIMARY KEY(heroId, powerId)
	
);
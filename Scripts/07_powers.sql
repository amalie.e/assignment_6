USE SuperheroesDb;

INSERT INTO Power(Name, Description) VALUES ('Almighty Push', 'Controlling gravity');
INSERT INTO Power(Name, Description) VALUES ('Rasengan', 'Wind ball of chakra');
INSERT INTO Power(Name, Description) VALUES ('Detroit Smash', 'Smashes really hard');
INSERT INTO Power(Name, Description) VALUES ('Sun breathing', 'Excessive breathing power');


INSERT INTO HeroPower(heroId, powerId) VALUES(1,3);
INSERT INTO HeroPower(heroId, powerId) VALUES(1,4);
INSERT INTO HeroPower(heroId, powerId) VALUES(2,3);
INSERT INTO HeroPower(heroId, powerId) VALUES(2,1);
INSERT INTO HeroPower(heroId, powerId) VALUES(3,1);
INSERT INTO HeroPower(heroId, powerId) VALUES(1,2);